define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for imgHeaderLogo **/
    AS_Image_i626ec9ec6254a2cb5e64abe31376db9: function AS_Image_i626ec9ec6254a2cb5e64abe31376db9(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("DashBoard");
        ntf.navigate();
    },
    /** onMapping defined for customerDetails **/
    AS_Form_cf2e23549aa24ba68b8da43650ac0700: function AS_Form_cf2e23549aa24ba68b8da43650ac0700(eventobject) {
        var self = this;

        function SHOW_ALERT_fd6e90b767e9451aaf4870136ce63e05_True() {}
        function INVOKE_ASYNC_SERVICE_gefb88fada87495bb69b72f6e82d57cd_Callback(status, getCustomerList) {
            kony.application.dismissLoadingScreen();
            if (getCustomerList.opstatus == 0) {
                var tempCollection331 = [];
                var tempData6645 = getCustomerList.body;
                for (var each217 in tempData6645) {
                    tempCollection331.push({
                        "lblLANGUAGE": {
                            "text": tempData6645[each217]["LANGUAGE"]
                        },
                        "lblTARGET": {
                            "text": tempData6645[each217]["TARGET"]
                        },
                        "lblNATIONALITY": {
                            "text": tempData6645[each217]["NATIONALITY"]
                        },
                        "lblACCOUNTOFFICER": {
                            "text": tempData6645[each217]["ACCOUNT.OFFICER"]
                        },
                        "lblMNEMONIC": {
                            "text": tempData6645[each217]["MNEMONIC"]
                        },
                        "lblINDUSTRY": {
                            "text": tempData6645[each217]["INDUSTRY"]
                        },
                        "lblSECTOR": {
                            "text": tempData6645[each217]["SECTOR"]
                        },
                        "lblCustomerId": {
                            "text": kony.visualizer.toString(tempData6645[each217]["CustomerId"])
                        },
                        "lblSTREET": {
                            "text": tempData6645[each217]["STREET"]
                        },
                        "lblCUSTOMERSTATUS": {
                            "text": tempData6645[each217]["CUSTOMER.STATUS"]
                        },
                        "lblSHORTNAME": {
                            "text": tempData6645[each217]["SHORT.NAME"]
                        },
                        "lblRESIDENCE": {
                            "text": tempData6645[each217]["RESIDENCE"]
                        },
                        "lblNAME1": {
                            "text": tempData6645[each217]["NAME.1"]
                        },
                        "lblgender": {
                            "text": tempData6645[each217]["gender"]
                        },
                    });
                }
                self.view.seggetCustomerList.setData(tempCollection331);
            } else {
                function SHOW_ALERT_fd6e90b767e9451aaf4870136ce63e05_Callback() {
                    SHOW_ALERT_fd6e90b767e9451aaf4870136ce63e05_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_INFO,
                    "alertTitle": null,
                    "yesLabel": null,
                    "noLabel": null,
                    "alertIcon": null,
                    "message": "Data fetch failed! Please try again later.",
                    "alertHandler": SHOW_ALERT_fd6e90b767e9451aaf4870136ce63e05_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        }
        kony.application.showLoadingScreen(null, null, constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, true, {});
        if (getCustomerList_inputparam == undefined) {
            var getCustomerList_inputparam = {};
        }
        getCustomerList_inputparam["serviceID"] = "CustomerCreationJSON$getCustomerList";
        getCustomerList_inputparam["customerId"] = self.view.Label0e4ba87941d6249.text;
        var getCustomerList_httpheaders = {};
        getCustomerList_inputparam["httpheaders"] = getCustomerList_httpheaders;
        var getCustomerList_httpconfigs = {};
        getCustomerList_inputparam["httpconfig"] = getCustomerList_httpconfigs;
        CustomerCreationJSON$getCustomerList = mfintegrationsecureinvokerasync(getCustomerList_inputparam, "CustomerCreationJSON", "getCustomerList", INVOKE_ASYNC_SERVICE_gefb88fada87495bb69b72f6e82d57cd_Callback);
    },
    /** preShow defined for customerDetails **/
    AS_Form_gb74b144f7a64426a008bd7524eea8f2: function AS_Form_gb74b144f7a64426a008bd7524eea8f2(eventobject) {
        var self = this;
        this.view.Label0e4ba87941d6249.text = myGlobal;
    }
});