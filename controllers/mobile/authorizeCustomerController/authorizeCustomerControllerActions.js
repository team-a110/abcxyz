define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onTouchEnd defined for Image0eb036225091c48 **/
    AS_Image_f0968c3b38ab4a5a97b24c9400a67ffe: function AS_Image_f0968c3b38ab4a5a97b24c9400a67ffe(eventobject, x, y) {
        var self = this;
        var ntf = new kony.mvc.Navigation("DashBoard");
        ntf.navigate();
    },
    /** onClick defined for btnSquared **/
    AS_Button_ba82d096389e40cc851fa0a010ebdbb3: function AS_Button_ba82d096389e40cc851fa0a010ebdbb3(eventobject) {
        var self = this;

        function SHOW_ALERT_d1316d2930ee44289f5e9639b5e904c1_True() {}
        function INVOKE_ASYNC_SERVICE_bf20921a0e7442189272854f9bf0b4a7_Callback(status, authorizeCustomer) {
            kony.application.dismissLoadingScreen();
            if (authorizeCustomer.opstatus == 0) {
                alert("You Have Successfully Authorize the Customer");
            } else {
                function SHOW_ALERT_d1316d2930ee44289f5e9639b5e904c1_Callback() {
                    SHOW_ALERT_d1316d2930ee44289f5e9639b5e904c1_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_INFO,
                    "alertTitle": null,
                    "yesLabel": null,
                    "noLabel": null,
                    "alertIcon": null,
                    "message": "No UNAUTH record found",
                    "alertHandler": SHOW_ALERT_d1316d2930ee44289f5e9639b5e904c1_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        }
        kony.application.showLoadingScreen(null, null, constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, true, {});
        if (authorizeCustomer_inputparam == undefined) {
            var authorizeCustomer_inputparam = {};
        }
        authorizeCustomer_inputparam["serviceID"] = "CustomerCreationJSON$authorizeCustomer";
        authorizeCustomer_inputparam["customerId"] = self.view.tbxcustomerId.text;
        var authorizeCustomer_httpheaders = {};
        authorizeCustomer_inputparam["httpheaders"] = authorizeCustomer_httpheaders;
        var authorizeCustomer_httpconfigs = {};
        authorizeCustomer_inputparam["httpconfig"] = authorizeCustomer_httpconfigs;
        CustomerCreationJSON$authorizeCustomer = mfintegrationsecureinvokerasync(authorizeCustomer_inputparam, "CustomerCreationJSON", "authorizeCustomer", INVOKE_ASYNC_SERVICE_bf20921a0e7442189272854f9bf0b4a7_Callback);
    }
});