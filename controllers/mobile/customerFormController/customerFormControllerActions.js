define({
    /*
      This is an auto generated file and any modifications to it may result in corruption of the action sequence.
    */
    /** onClick defined for btnSquared **/
    AS_Button_jde5d4777b9e4992ac71d3c90d39ec73: function AS_Button_jde5d4777b9e4992ac71d3c90d39ec73(eventobject) {
        var self = this;

        function SHOW_ALERT_dc5b26e07958447a845edd23be375762_True() {}
        function INVOKE_ASYNC_SERVICE_b51e101fdd704cfa8d2959aedfecf975_Callback(status, CustomerCreation) {
            kony.application.dismissLoadingScreen();
            if (CustomerCreation.opstatus == 0) {
                alert("Congratulations!! Customer ID is  :" + JSON.stringify(CustomerCreation.header.id));
                var ntf = new kony.mvc.Navigation("authorizeCustomer");
                ntf.navigate();
            } else {
                function SHOW_ALERT_dc5b26e07958447a845edd23be375762_Callback() {
                    SHOW_ALERT_dc5b26e07958447a845edd23be375762_True();
                }
                kony.ui.Alert({
                    "alertType": constants.ALERT_TYPE_INFO,
                    "alertTitle": null,
                    "yesLabel": null,
                    "noLabel": null,
                    "alertIcon": null,
                    "message": "Data fetch failed! Please try again later.",
                    "alertHandler": SHOW_ALERT_dc5b26e07958447a845edd23be375762_Callback
                }, {
                    "iconPosition": constants.ALERT_ICON_POSITION_LEFT
                });
            }
        }
        kony.application.showLoadingScreen(null, null, constants.LOADING_SCREEN_POSITION_FULL_SCREEN, true, true, {});
        if (CustomerCreation_inputparam == undefined) {
            var CustomerCreation_inputparam = {};
        }
        CustomerCreation_inputparam["serviceID"] = "CustomerCreationJSON$CustomerCreation";
        CustomerCreation_inputparam["Nationality"] = self.view.tbxNationality.text;
        CustomerCreation_inputparam["customerMnemonic"] = self.view.tbxcustomerMnemonic.text;
        CustomerCreation_inputparam["customerName"] = self.view.tbxcustomerName.text;
        CustomerCreation_inputparam["displayName"] = self.view.tbxdisplayName.text;
        CustomerCreation_inputparam["language"] = kony.visualizer.toNumber(self.view.tbxlanguage.text);
        CustomerCreation_inputparam["sectorId"] = kony.visualizer.toNumber(self.view.tbxsectorId.text);
        CustomerCreation_inputparam["gender"] = self.view.tbxgender.text;
        CustomerCreation_inputparam["customerstatus"] = kony.visualizer.toNumber(self.view.tbxcustomerstatus.text);
        var CustomerCreation_httpheaders = {};
        CustomerCreation_inputparam["httpheaders"] = CustomerCreation_httpheaders;
        var CustomerCreation_httpconfigs = {};
        CustomerCreation_inputparam["httpconfig"] = CustomerCreation_httpconfigs;
        CustomerCreationJSON$CustomerCreation = mfintegrationsecureinvokerasync(CustomerCreation_inputparam, "CustomerCreationJSON", "CustomerCreation", INVOKE_ASYNC_SERVICE_b51e101fdd704cfa8d2959aedfecf975_Callback);
    },
    /** onTextChange defined for tbxNationality **/
    AS_TextField_c15ab4ff14a1446eb50a3281ba1cd3a9: function AS_TextField_c15ab4ff14a1446eb50a3281ba1cd3a9(eventobject, changedtext) {
        var self = this;
        this.view.tbxcustomerName.text.toUpperCase();
    }
});